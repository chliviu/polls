
def details(request):
    return render(request, 'login.html')

def change(request):
    username = request.POST.get('username')
    password = request.POST.get('password')

    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)
        return redirect('/')
    else:
        return render(request, 'login.html', {'login_failed': True})

def page(request):
    question_id = kwargs.get('id')
    try:
        # get question
        question_obj = Question.objects.get(id=question_id)
    except ObjectDoesNotExist:
        raise Http404('question id does not exist')

    # get choices
    choices = Choice.objects.filter(question=question_obj)
    return render(request, 'vote_page.html', {'question': question_obj, 'choices': choices})
